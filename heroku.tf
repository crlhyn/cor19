provider "heroku" {
  version = "~> 2.0"
}

#HEROKU_API_KEY=<TOKEN> HEROKU_EMAIL=<EMAIL>

variable "app_name" {
  description = "Name of the Heroku app provisioned as an example"
}

resource "heroku_app" "example" {
  name   = "${var.app_name}"
  region = "us"
}

