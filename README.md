# cor19  dashboard

A time-series dashboard suitable for data pipelines and system metrics. 

## Deliverables
  -	D1 Implementation architecture
      ![alt architecture doc d1](arch.png "Architecture D1")
  -	D2 Working system on build and Q&A accesible servers
    -  pointing features, and how each one met some requirements .
    - This description of deliverables and 	
    - time took the completion of each deliverable in iteraction
  -  D3 Test harness that can generate and send push updates to the dashboard to mimic the type of data the operational system would send.
      - This harness was used to demonstrate & test the dashboard system.
      - The scripts and examples serves as a reference implementation for updating the dashboard that we can use when adding dashboard-update commands to the new or existing integration scripts, or when implementing a monitoring script that needs to update the dashboard.
	  - The example files show the type and frequency of updates for some integrations, allowing the harness to produce a representative example.
  - D4 Deployed and tested system in Secured site Production
      - same url as in QA site (D2) was deployed on the secured domain
         
         
## Features
 
 - f1	The current dashboard systems remain in active use in the Architecture
 - f2	This createded a new dashboard, implemented using the grafana system and influx (+opetional) databases. Able to run in parallel to the existing system.
 - f3	It is also running in the secered domain, verified viewable from any Secured Workstation without the need for an Admin priviledges nor tunnels.       
 - f4	The dashboard support  data being pushed to it, hence with fewest and clearest dependencies. Pushing the data is as simple as an atomic HTTP POST. 
   -  Pushed updates verified both from systems scripts themselves, as well as by some monitoring process that periodically scans or subscribe to existing event logs.
   -  Pushed updates are atomic and only include current value and target board/label. The dashboard adds the timestampt (unless optionally provided) and stores the time-series.
   -  we refer to the mining and harnessing systems that allows also query data by eriodically scanning or subscribing to events.
 - f5	Pipelines code sending to this dashboard is recommended to verify the code examples and use not blocking and independent even if dashboard process stops responding, 
     - following those recommendations ,  the monitor did not create any significant load on the application servers.
     - For an integration script, this means it should not increase running time by more than 10%.           
  -  f6 Visualization .      	Each time series is displayed as a simple line plot.
  - f7	The dashboard and widgets are sized in 3x3 different panels that can fit readably on the screen at once. (crtl - + to fit, kiosk mode )
     - this	Includes clearly visible labels so it’s easy to tell at a distance which plot corresponds to which pipeline or system.
     - additionally: a backup set of another 3x3 panels (feedding from backup databases) is below it ( scroll in case of main DB failure )
  - f8 	Each time series display is independently updatable.  ( grafana let us chose colors in addition of the homogeneous set chosen )
  -  f9 Each separate time series should show the last ~10 runs that have data.
        - 	Each integration runs periodically – as frequently as every 2 minutes and as rarely as weekly
        - 	Not every run results in data – in some cases the vast majority of runs are no-ops and will update the dashboard with a value of 0. 
        - 	Ideally the dashboard would be smart enough to auto-adjust the time-scale for each time series, but it is acceptable if we have to manually configure the scale for each time series.
  - f10	Each time series monitors a single data pipeline or system metric, but:
       - Some time series panels show more than one line plot: For instance, when useful to display status update events with time. Or when makes sense, the data pipeline can show 1 line on number of items received and another line on items processed 
       - Grafana let us add also other frequency and stats values as separate lines on the same graph. Several options were experimented with min, max, averages, ... with real data and outputs.
            
            